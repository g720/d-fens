
//Parallax section START
let controller = new ScrollMagic.Controller();
let timeline = new TimelineMax();

//Take the different image-assets and assign it different animations
timeline
  .fromTo('.helifront', { y: -150 }, { y: 100, duration: 8})
  .fromTo('.heliside', { y: -220 }, { y: 75, duration: 8},"-=8")
  .fromTo(".background", { y: -70 }, {y: 0, duration:8},"-=8")
  .to(".profiles-section", 8, { top: "10px" }, "-=8");


//Instead of timebased animation flow, use scrollmagic to base the animation on scroll. With the use of tweening-method setTween() we get a nice looking illusion of movement 
let scene = new ScrollMagic.Scene({
  triggerElement: ".about-wrapper",
  duration: "210%",
  triggerHook: 0,
  
})
  .setTween(timeline)
  .setPin(".about-wrapper")
  .addTo(controller);
//Parallax section END


//Employee/Modal section START

  
const profiles = [
	{
	  name: "Donatello",
	  imgsrc: "./Images/about_gallery/placeholderimage1.jpg",
	  clickme: "Click on me for info!"
	},
	{
	  name: "Leonardo",
	  imgsrc: "./Images/about_gallery/placeholderimage2.jpg",
	  clickme: "Click on me for info!"
	},
	{
	  name: "Michelangelo",
	  imgsrc: "./Images/about_gallery/placeholderimage3.jpg",
	  clickme: "Click on me for info!"
	},
	{
	  name: "Raphael",
	  imgsrc: "./Images/about_gallery/placeholderimage4.jpeg",
	  clickme: "Click on me for info!"
	}
  ]

const modals = [
	{
	  name: "Donatello",
	  header: "Lorem ipsum dolor, sit amet consectetur",
	  text: "Quis officia tenetur consequuntur rerum dolores eveniet!"
	},
	{
	  name: "Leonardo",
	  header: "repellat ea molestiae reprehenderit",
	  text: "Dolore magni tempore, nesciunt ex magnam ullam, sint officia eos beatae reiciendis asperiores?"
	},
	{
	  name: "Michelangelo",
	  header: "nesciunt ex magnam ullam",
	  text: "impedit rem est dicta Lorem ipsum dolor sit amet consectetur adipisicing elit.!"
	},
	{
	  name: "Raphael",
	  header: "reprehenderit ab quia nihil, ut vel et",
	  text: "similique dolorum reiciendis rem consectetur veniam. Fuga necessitatibus debitis dolorem maxime reprehenderit? "
	}
  ]

/* Profiles wrapper edited via javascript instead of hardcoding in HTML, adding the imgsrc from my profiles to src on the created images, also add names from profiles as id
*/
document.querySelector("#profiles-wrapper").innerHTML = profiles.map(profile => `
<div class="profile">
<img src=${profile.imgsrc} id=${profile.name}></img>
<h3 class="imageText">Click me!</h3>
</div>
`).join("")
  
/*Modals wrapper edited via javascript instead of hardcoding in HTML, same as the profiles. Taking properties from my modal object and writing it out h2 and p-elements*/
document.querySelector("#modals-wrapper").innerHTML = modals.map(modal => `
  <div class="modal">
  <span class="close">&times;</span>
  <div>
  <h2>${modal.name}</h2>
  <p>${modal.text}</p>
  </div>
</div>
`).join("")

//Get all modals
var modal = document.getElementsByClassName("modal");

//Get the elements that close the modals
var close = document.getElementsByClassName("close");

//Get all images
var imgs = [];
profiles.forEach((obj,index) => {
	imgs[index] = document.getElementById(obj.name);
})

//Make all the images clickable, open corresponding modal on click
imgs.forEach((img,index) =>{
	img.onclick = function(){
		modal[index].style.display = "flex";
	}
})

//Close the corresponding modal with display property set to none, when clicking the "X" in the top right corner
for(let i = 0; i < close.length; i++){
	close[i].onclick = function(){
		modal[i].style.display = "none";
	}
}



// Close the modal when clicking anywhere but the contentarea
window.onclick = function(event) {
	for(let i = 0; i < modal.length; i++){
		if (event.target == modal[i]) {
				modal[i].style.display = "none";
			}
	}
}


//Employee/Modal section END
