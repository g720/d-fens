//Var - Keeps count of which image that is currently showing in the heroGallerySlideShow.
let centerImgCounter = 0;

//Var - Stores all HTML-elements we need to manipulate in the JavaScript-file associated with heroGallery.
const heroRightBtn = document.querySelector(".hero-right-btn");
const heroLeftBtn = document.querySelector(".hero-left-btn");

const heroBottomBtnContainer = document.querySelector(".hero-bottom-btn-container");

const listOfBottomBtn = [];

const centerHeroImg = document.querySelector(".hero-center-img");
const rightHeroImg = document.querySelector(".hero-right-img");
const leftHeroImg = document.querySelector(".hero-left-img");

const heroImgCointainer = document.querySelector(".hero-image-container");

const heroHTwo = document.querySelector(".hero-overlay-h2");

//Array - of objects that contains heroImgUrl, heroHeaderText and heroParagraph - Dynamic: If you'll add a object to this list. The heroSlideShow will expand and work whitout any extra changes.
const heroImgGalleryList = [{'url': './Images/gallery/air.jpg', 'headerText': 'This is text 1'}, 
{'url': './Images/gallery/confidential.jpg', 'headerText': 'This is text 2'}, 
{'url': './Images/gallery/defence.jpg', 'headerText': 'This is text 3'}, 
{'url': './Images/gallery/logistic.jpg', 'headerText': 'This is text 4'}, 
{'url': './Images/gallery/tech.jpg', 'headerText': 'This is text 5'},
//Uncomment to test the dynamic: {'url': 'https://picsum.photos/1600', 'headerText': 'This is text 6'}
];

//Statement - Set the startingpoint of heroImg, background and header based on the array above. This is to get rid of hardcoded values in the HTML-document.
centerHeroImg.setAttribute('src', heroImgGalleryList[centerImgCounter].url);
heroHTwo.innerText = heroImgGalleryList[centerImgCounter].headerText;
heroImgCointainer.style.backgroundImage = `url(${heroImgGalleryList[centerImgCounter].url})`;
resetHeroImgOrder(centerImgCounter);

//Statement - For every object in heroImgGalleryList. -> Create and append a new heroBottomBtn with an event that runs setHeroImageWBottomB(<indexNr of heroImgGalleryList>): This will 
//associate a img to every button. -> Add the heroBottomBtn to listOfBottomBtn: So that we can manipulate all heroBottomBtns later on.
heroImgGalleryList.forEach((heroImg, heroImgIndex) => {
    let element = document.createElement("div");
    element.classList.add("hero-bottom-btn");
    element.setAttribute("onclick", `setHeroImageWBottomBtn(${heroImgIndex})`);
    heroBottomBtnContainer.appendChild(element);
    listOfBottomBtn.push(element);
});

//Statement - Adds a eventlistener to the right and left heroBtns. The eventlistener will call slideImgFrom(left/right). With these buttons we will be able to change heroImg 
//to the next or previous img in heroImgGalleryList.
heroRightBtn.addEventListener('click', () => {slideImgFromRight()});
heroLeftBtn.addEventListener('click', () => {slideImgFromLeft()});

//Statement - Even if no button is clicked, our slideshow needs to run. With setInterval we instructs that every 10s we will run slideImgFromRight.
setInterval(slideImgFromRight, 10000);

//Function - Based on which heroBottomBtn that is pressed -> Change the centerHeroImg to the heroBottomBtn-associated-img with help of indexNr and update centerImgCounter so that we know which img we are showing. 
//1. Change background of heroImgContainer to new img. 
//2. Fade(CSS transition) current img to opacity 0. New img is now showing but as background of heroImgContainer.
//3. Disable all heroButtons to prevent speed clicking which will make everything look wierd. "Color" the new img associated bottomBtn. Change the associated header and paragraph. 
//4. Change the centerHeroImg to match the background then make it visible. Enable all hero buttons.
//5. Change leftHeroImg and rightHeroImg to be next and previous heroIMG.: Preperation for next slide.
function setHeroImageWBottomBtn(imgIndex){
    centerImgCounter = imgIndex;
    heroImgCointainer.style.backgroundImage = `url(${heroImgGalleryList[imgIndex].url})`; 
    centerHeroImg.style.opacity = "0";
    disableAllHeroBtns();
    setBottomBtnsImgMarker(imgIndex);
    changeHeroText(imgIndex);
    setTimeout(() => {centerHeroImg.setAttribute('src', heroImgGalleryList[imgIndex].url); centerHeroImg.style.opacity = "1";}, 500);
    setTimeout(() => {enableAllHeroBtns();}, 1000);
    resetHeroImgOrder(imgIndex);
}

//Function - Disable buttons to prevent speedclick. Slide in(with translateX and CSS-transition) the rightImg. Set centerImgCounter based on where in the list we are. If we are at the end
//of the list -> set to 0. Else set to +1 previous value. Change the heroText. "Color" the bottomBtn. Set right and left img based on centerImg and then enable all buttons. 
function slideImgFromRight(){
    disableAllHeroBtns();
    rightHeroImg.style.visibility = "visible"
    rightHeroImg.style.transform = "translateX(0vw)";
    if(centerImgCounter >= heroImgGalleryList.length - 1){
        centerImgCounter = 0;
        console.log(centerImgCounter);
    } else {
        centerImgCounter++;
        console.log(centerImgCounter);
    }
    changeHeroText(centerImgCounter);
    setBottomBtnsImgMarker(centerImgCounter);
    resetHeroImgOrder(centerImgCounter);
    setTimeout(() => {enableAllHeroBtns();}, 1000);
}

//Function - Disable buttons to prevent speedclick. Slide in(with translateX and CSS-transition) the leftImg. Set centerImgCounter based on where in the list we are. If we are at the start(0)
//of the list -> set to the last picture in the list. Else set to -1 previous value. Change the heroText. "Color" the bottomBtn. Set right and left img based on centerImg and then enable all buttons. 
function slideImgFromLeft(){
    disableAllHeroBtns();
    leftHeroImg.style.visibility = "visible"
    leftHeroImg.style.transform = "translateX(0vw)";

    if(centerImgCounter <= 0){
        centerImgCounter = heroImgGalleryList.length - 1;
        console.log(centerImgCounter);
    } else {
        centerImgCounter--;
        console.log(centerImgCounter);
    }
    changeHeroText(centerImgCounter);
    setBottomBtnsImgMarker(centerImgCounter);
    resetHeroImgOrder(centerImgCounter);

    setTimeout(() => {enableAllHeroBtns();}, 1000);
}

//Function - Disable all buttons in hero by adding inline-css.
function disableAllHeroBtns(){
    heroRightBtn.style.pointerEvents = "none";
    heroLeftBtn.style.pointerEvents = "none";
    listOfBottomBtn.forEach(btn => {btn.style.pointerEvents = "none";});
}

//Function - Enable all buttons in hero by changing inline-css.
function enableAllHeroBtns(){
    listOfBottomBtn.forEach(btn => {btn.style.pointerEvents = "auto";});
    heroRightBtn.style.pointerEvents = "auto";
    heroLeftBtn.style.pointerEvents = "auto";
}

//Function - Set headerText based on centerImg. heroStringIndex param will be centerImgCounter.
function changeHeroText(heroStringIndex){
    heroHTwo.style.opacity = "0";
    setTimeout(() => {heroHTwo.innerText = heroImgGalleryList[heroStringIndex].headerText; heroHTwo.style.opacity = "1";}, 500);
}

//Function - "Color" bottomBtn that is associated with centerImg. First uncolor all, then color the correct one. imgNBtnIndex will be centerImgCounter.
function setBottomBtnsImgMarker(imgNBtnIndex){
    listOfBottomBtn.forEach(btn => {btn.style.backgroundColor = "white";});
    listOfBottomBtn[imgNBtnIndex].style.backgroundColor = "black";
}

//Function - Sets right and left img that will be used to slide in new images. If centerImg is at list-end - Set right to 1st listItem and left to -1.
//If centerImg is at list-start - Set right to +1 and left to last list-item.
//Set the centerImg to be the img that has been sliding in.
//We will also make left/right img hidden and move back to the right space.
function resetHeroImgOrder(centerImgIndex){
    let rightImg = (() => {
        let returnThis = centerImgIndex + 1;
        if(returnThis >= heroImgGalleryList.length){
            returnThis = 0;
        }
        return returnThis;
    })();

    let leftImg = (() => {
        let returnThis = centerImgIndex - 1;
        if(returnThis < 0){
            returnThis = heroImgGalleryList.length - 1;
        }
        return returnThis;
    })();

    setTimeout(() => {centerHeroImg.setAttribute('src', heroImgGalleryList[centerImgIndex].url); leftHeroImg.style.visibility = "hidden"; rightHeroImg.style.visibility = "hidden"; leftHeroImg.style.transform = "translateX(-100vw)"; rightHeroImg.style.transform = "translateX(100vw)";}, 500);
    setTimeout(() => {leftHeroImg.setAttribute('src', heroImgGalleryList[leftImg].url); rightHeroImg.setAttribute('src', heroImgGalleryList[rightImg].url);}, 800);
}

const lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."

//Array - Containing all gallery Images(Not herogallery). Dynamic: Add URL to this array and everything will work.
const galleryArray = [{"url":"https://picsum.photos/800", "header":"Header 1", "content":`${lorem}`}, {"url":"https://picsum.photos/801", "header":"Header 2", "content":`${lorem}`}, {"url":"https://picsum.photos/802", "header":"Header 3", "content":`${lorem}`}, {"url":"https://picsum.photos/803", "header":"Header 4", "content":`${lorem}`}, {"url":"https://picsum.photos/804", "header":"Header 5", "content":`${lorem}`}, {"url":"https://picsum.photos/799", "header":"Header 6", "content":`${lorem}`}, {"url":"https://picsum.photos/798", "header":"Header 7", "content":`${lorem}`}, {"url":"https://picsum.photos/797", "header":"Header 8", "content":`${lorem}`}, {"url":"https://picsum.photos/796", "header":"Header 9", "content":`${lorem}`}, {"url":"https://picsum.photos/795", "header":"Header 10", "content":`${lorem}`}, {"url":"https://picsum.photos/794", "header":"Header 11", "content":`${lorem}`}, {"url":"https://picsum.photos/793", "header":"Header 12", "content":`${lorem}`}, {"url":"https://picsum.photos/792", "header":"Header 13", "content":`${lorem}`}, {"url":"https://picsum.photos/791", "header":"Header 14", "content":`${lorem}`}, {"url":"https://picsum.photos/790", "header":"Header 15", "content":`${lorem}`}, {"url":"https://picsum.photos/805", "header":"Header 16", "content":`${lorem}`}, {"url":"https://picsum.photos/806", "header":"Header 17", "content":`${lorem}`}, {"url":"https://picsum.photos/807", "header":"Header 18", "content":`${lorem}`}];

//Var - Stores all HTML-elements we need to manipulate in the JavaScript-file associated with the modal-gallery.
const modalGallery = document.querySelector(".modal-gallery");
const modalGalleryThumbnailContainer = document.querySelector(".modal-gallery-thumbnail-container");
const galleryThumbnailContainer = document.querySelector(".gallery-thumbnail-container");
const modalGalleryImg = document.querySelector(".modal-gallery-img");

//Statement - Creates the initial set-up of gallery in both regular site and modal. Show all img in thumbnail-container.
//We will also give each thumbnail a ID of ..... + indexNr and a common class which will make it easy to target a single thumbnail or all together.
galleryArray.forEach((img, arrayIndex) => {
    if(arrayIndex%2 == 0){
    let divElement = document.createElement("div");
    divElement.innerHTML = `<h3>${img.header}</h3><p>${img.content}</p>`;
    galleryThumbnailContainer.appendChild(divElement);
        
    let element = document.createElement("img");
    element.setAttribute("alt", "Thumbnailpic");
    element.setAttribute("src", img.url);
    element.setAttribute("id", `thumbnail${arrayIndex}`);
    element.classList.add("thumbnail-gallery-pic");
    element.setAttribute("onclick", "openModalGalleryImg(this.id)");
    galleryThumbnailContainer.appendChild(element);

    } else{
        
    let element = document.createElement("img");
    element.setAttribute("alt", "Thumbnailpic");
    element.setAttribute("src", img.url);
    element.setAttribute("id", `thumbnail${arrayIndex}`);
    element.classList.add("thumbnail-gallery-pic");
    element.setAttribute("onclick", "openModalGalleryImg(this.id)");
    galleryThumbnailContainer.appendChild(element);

    let divElement = document.createElement("div");
    divElement.innerHTML = `<h3>${img.header}</h3><p>${img.content}</p>`;
    galleryThumbnailContainer.appendChild(divElement);
    }

    let modalElement = document.createElement("img");
    modalElement.setAttribute("alt", "Modal-Thumbnailpic");
    modalElement.setAttribute("src", img.url);
    modalElement.setAttribute("id", `modal-thumbnail${arrayIndex}`);
    modalElement.classList.add("modal-thumbnail-gallery-pic");
    modalElement.setAttribute("onclick", "setModalGalleryImg(this.id)");
    modalGalleryThumbnailContainer.appendChild(modalElement);
});

//Function - This function runs when any thumbnailImg in the regular site is pressed. It sends its elements ID. With the ID we'll get the imageURL and set the modalBigPicture to the same img that was clicked.
//We will also create a border around the clickedImg but in the modalThumbnails instead. To do this we will use a regEx and get the number of the ID and create the ID of the modalThumbnails.
//The modalThumbnails and regular thumbnails will hade different text but same number because both is based of same array.
//Before we do this we remove all yellowborders. There should only be one thumbnail with a yellow border.
//We'll also set the bodys Y overflow to hidden. This is because i dont want to have a scroll on the body. When modal is open the user wont be able to affect the "regular site";
function openModalGalleryImg(clickedId){
    modalGallery.style.display = "block";
    let clickedIdUrl = document.querySelector(`#${clickedId}`).getAttribute("src");
    modalGalleryImg.setAttribute("src", clickedIdUrl);
    let index = clickedId.match(/(\d+)/);
    let modalId = `modal-thumbnail${index[0]}`;
    document.querySelectorAll(`.modal-thumbnail-gallery-pic`).forEach((img) => {
        img.classList.remove("gallery-yellow-border");
    });
    document.querySelector(`#${modalId}`).classList.add("gallery-yellow-border");
    document.querySelector("body").style.overflowY = "hidden";
}

//Function - This function runs when any thumbnailImg in the modal is pressed. Remove yellow borders. Set bigImg to clickedURL with help of id. Set yellow border with help of ID.
function setModalGalleryImg(clickedId){
    document.querySelectorAll(`.modal-thumbnail-gallery-pic`).forEach((img) => {
        img.classList.remove("gallery-yellow-border");
    });
    let clickedIdUrl = document.querySelector(`#${clickedId}`).getAttribute("src");
    document.querySelector(`#${clickedId}`).classList.add("gallery-yellow-border");
    modalGalleryImg.setAttribute("src", clickedIdUrl);
}

//Closes modal and enables scroll on body
function closeGalleryModal(){
    document.querySelector("body").style.overflowY = "auto";
    modalGallery.style.display = "none";
}

function moveModalImgRight(){
    let currentUrl = modalGalleryImg.getAttribute("src");
    console.log(currentUrl);

    let currentID;
    document.querySelectorAll(`.modal-thumbnail-gallery-pic`).forEach((img) => {
        if(img.getAttribute("src") === currentUrl){
            currentID = img.getAttribute("id");
        }
    });
    let thisIndex = currentID.match(/(\d+)/);
    let nextIndex = 0;
    if(thisIndex[0] >= galleryArray.length -1){
        nextIndex = 0;
    } else{
        nextIndex = parseInt(thisIndex[0]) + 1;
    }
    let nextID = `modal-thumbnail${nextIndex}`
    modalGalleryImg.setAttribute("src", `${document.querySelector(`#${nextID}`).getAttribute("src")}`);
    document.querySelectorAll(`.modal-thumbnail-gallery-pic`).forEach((img) => {
        img.classList.remove("gallery-yellow-border");
    });
    document.querySelector(`#${nextID}`).classList.add("gallery-yellow-border");
}

function moveModalImgLeft(){
    let currentUrl = modalGalleryImg.getAttribute("src");
    console.log(currentUrl);

    let currentID;
    document.querySelectorAll(`.modal-thumbnail-gallery-pic`).forEach((img) => {
        if(img.getAttribute("src") === currentUrl){
            currentID = img.getAttribute("id");
        }
    });
    let thisIndex = currentID.match(/(\d+)/);
    let nextIndex = 0;
    if(thisIndex[0] <= 0){
        nextIndex = galleryArray.length -1;
    } else{
        nextIndex = parseInt(thisIndex[0]) - 1;
    }
    let nextID = `modal-thumbnail${nextIndex}`
    modalGalleryImg.setAttribute("src", `${document.querySelector(`#${nextID}`).getAttribute("src")}`);
    document.querySelectorAll(`.modal-thumbnail-gallery-pic`).forEach((img) => {
        img.classList.remove("gallery-yellow-border");
    });
    document.querySelector(`#${nextID}`).classList.add("gallery-yellow-border");
}