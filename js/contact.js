// Erik's Javascript

//variabel för html-form
const inputForm = document.getElementById('form')

//variabler för användarens input
const inputSubject = document.getElementById('subject')
const inputEmail = document.getElementById('email')
const inputMessage = document.getElementById('message')
const errorSubjectMessages = document.getElementById('errorSubject')
const errorEmailMessages = document.getElementById('errorEmail')
const errorMessageMessages = document.getElementById('errorMessage')

//modal variabler
let modal = document.getElementById('contact-form-modal');
let modalContent = document.getElementsByClassName('contact-form-modal-content');
let btn = document.getElementById('submit');
let closeBtn = document.getElementsByClassName('close')[0];
let modalGreetingMessage = document.getElementById('.modal-text-header');
let modalGreetingText = document.getElementById('modal-text-paragraph');
let spinner = document.getElementById('icon-spinner');


//variabel för att kontrollera följderna av en e-mail, hittad på nätet.
const checkEmailValidity = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

//eventlistener för submit knappen
inputForm.addEventListener('submit', (e) =>{
    let subjectMessages = [] //subject meddelanden som beror på hur användaren har skrivit pushas hit
    let emailMessages = [] //email meddelanden som beror på hur användaren har skrivit pushas hit
    let messageMessages = [] //message meddelanden som beror på hur användaren har skrivit pushas hit

    //Variabler för att hålla kolla på användaren's input
    let subjectIsValid = false;
    let emailIsValid = false;
    let messageIsValid = false;

    
    //Email
    //Testar email för regular expression som ligger i variabeln checkEmailValidity, pushar Valid Email meddelande till array
    if (checkEmailValidity.test(inputEmail.value)) {
        emailMessages.push ("Valid email.")
        emailIsValid = true;
    }   
    //Ifall email inte stämmer överens med regular expression pushas meddelande till array som sparar meddelanden 
    else if (emailIsValid == false) {
        emailMessages.push ("Invalid email.")
    }
    
    //Subject
    //Testar ifall subject är tom, isåfall pushas ett felmeddelande till array som sparar meddelanden
    if (inputSubject.value === '' || inputSubject.value == null){
        subjectMessages.push("Subject is required.")
    }
    //Ifall subject inte är tom, pushas Great subject till array som sparar meddelanden
    else {
        subjectMessages.push("Great subject!")
        subjectIsValid = true;
    }

    //Message
    //Ifall message variabeln är tom, pushas meddelandet Message is required till array
    if (inputMessage.value === '' || inputMessage.value == null){
        messageMessages.push("Message is required.")
    }
    //Ifall message variabeln ej är tom, pushas meddelandet Wonderful message till array
    else {
        messageMessages.push ("Wonderful message!")
        messageIsValid = true;
    }
    
    //Printar ut felmeddelanden sparade i arrays
    if (subjectMessages.length > 0 && emailMessages.length > 0 && messageMessages.length > 0) { 
        errorSubjectMessages.innerText = subjectMessages.join();
        errorEmailMessages.innerText = emailMessages.join();
        errorMessageMessages.innerText = messageMessages.join();
    }
    //Hindrar submit från att utföras om något av användarens input är invalid
    if (!emailIsValid || !subjectIsValid || !messageIsValid) {
        e.preventDefault();
    }

    //Öppnar modal ifall user-input variablerna är true
 
    if (emailIsValid && subjectIsValid && messageIsValid) {
        modal.style.display = "block";
        
        //onclick för att stänga ner modal
        closeBtn.onclick = function () {
            modal.style.display = "none";
            inputEmail.value = "";
            inputSubject.value = "";
            inputMessage.value = "";
            errorEmailMessages.innerText = "";
            errorSubjectMessages.innerText = "";
            errorMessageMessages.innerText = "";
        }
        e.preventDefault();
    }   
})



















